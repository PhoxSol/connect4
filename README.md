# Connect 4 #

## Submission Instructions ##

Thank you for taking the time to inspect my submission.
This has been written upon the node.js framework, requiring an [install of NPM and node.js](https://nodejs.org/en/) in order to run.

Once installed, and this repo has been pulled, run:

npm install

... from within the directory, to install the dependancies.
If any warnings appear, they are acceptable, as this is a non-production project.

To serve the page, run:

npm start

... and navigate to localhost:3000 to view.

Enjoy!


## Testing Instructions ##

## Summary ##
Your exercise will be to create a web version of the classic game of Connect4. Our board consists of 6 columns with 7 rows high. At the start of a new game, the board is empty. If you have never played Connect4, there are plenty of explanations out on the Internet.

## Objective ##

Be the first player to get four of your colored checkers in a row - horizontally, vertically, or diagonally.

## How to play ##

1.  Red or yellow player starts. Players will alternate turns after playing a checker. Each player will be prompted that it is his or her turn by "[Red]/[Yellow], please enter the column to drop your checker:". If you prefer, you can have a drag/drop interface.
2.  On your turn, you specify which column to drop your checker when prompted. The checker will "fall" to the lowest point in that column. This does not need to have any fancy UI elements (i.e. animation), but again, if you want to, feel free to put those in there.
3.  Following a players move, there will either be a prompt for the next player's move, or the website will display "[Red]/[Yellow] wins!" if there is a winner.
4.  After there is a winner, the game will prompt, "Do you want to play again?". The loser of the previous game will the first player of the next game if the players choose to play again. Otherwise, the website thanks you for playing.

## Expectations ##

-   The game will be started by running the website in any modern browser.
-   If the user gives improper input, feedback is given to the user to try again.

## Other things to remember ##
- We want to see what you know. So, if you feel more comfortable doing back-end work, then focus a lot of your energy on creating a beautiful back end code base with a functional front-end experience. However, if you feel more comfortable doing front-end work, then focus on creating a beautiful front-end for the website, and create a functional back-end. We really want to see you shine!
- Remember that we want to be able to see your work in action, so make sure that it runs.
- Unit Testing is not required for this, but if you want to show us what you know about unit testing, that would be "bonus"
- When you are finished, please issue a pull request and then let us know that you've completed the exercise.
- Have fun with this exercise!